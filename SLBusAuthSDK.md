# SLBusAuth Android SDK Documentation

## Setup

#### Declare the following permissions in the Manifest:
```
<uses-permission android:name="android.permission.RECORD_AUDIO" />
<uses-permission android:name="android.permission.INTERNET" />
<uses-permission android:name="android.permission.ACCESS_NETWORK_STATE"/>
```

#### Declare the BusAuthMessagingService in the Manifest:
```
<service
    android:name="com.silencelaboratories.busauthsdk.BusAuthMessagingService"
    android:exported="false">
    <intent-filter>
        <action android:name="com.google.firebase.MESSAGING_EVENT" />
    </intent-filter>
</service>

```
or if your app is already using Firebase messaging:
```
<service
    android:name=".FirebaseMessagingService"
    android:exported="false">
    <intent-filter>
        <action android:name="com.google.firebase.MESSAGING_EVENT" />
    </intent-filter>
</service>
```
and add changes to FirebaseMessagingService.java
```
import com.silencelaboratories.busauthsdk.BusAuthMessagingService;
import static com.silencelaboratories.busauthsdk.BusAuthSDK.FCM_PROJECT_NUMBER;

public class MyFirebaseMessagingService extends BusAuthMessagingService {

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        if (remoteMessage.getFrom().equals(FCM_PROJECT_NUMBER)) {
            super.onMessageReceived(remoteMessage);
        } 
        else {
            // process message
            ...
        }
    }
    ...
}
```

## SDK methods

#### Create and initialize a BusAuthSDK instance
```
BusAuthSDK busAuthSDK = BusAuthSDK.getInstance(context);
busAuthSDK.init();
```
#### Check the account status
```
int result = busAuthSDK.checkAccount();
result:
    BusAuthSDK.ACCOUNT_EXIST
    BusAuthSDK.ACCOUNT_NOT_ADDED_YET
```
#### Add new account by *activation_code*
```
int result = busAuthSDK.addAccount(activation_code);
result:
    BusAuthSDK.ACCOUNT_ADDED_SUCCESSFULLY
    BusAuthSDK.ACTIVATION_CODE_INVALID
    BusAuthSDK.NETWORK_ERROR
```
#### Remove the account
```
busAuthSDK.removeAccount();
```

#### Start BusAuthSDK
```
int result = busAuthSDK.start();
result:
    BusAuthSDK.SUCCESS
    BusAuthSDK.ACCOUNT_NOT_ADDED_YET
```

#### Get account ID and Type
```
String accountId = busAuthSDK.getAccountId();
int accountType = busAuthSDK.getAccountType();
accountType:
    BusAuthSDK.ACCOUNT_TYPE_PASSENGER
    BusAuthSDK.ACCOUNT_TYPE_DRIVER
    BusAuthSDK.ACCOUNT_NOT_ADDED_YET
```

#### Create a new ride for a Passenger
```
int result = busAuthSDK.passengerNewRide(String passengerId,
                                         String driverId,
                                         String boardingTime)
result:
    BusAuthSDK.SUCCESS
    BusAuthSDK.ACCOUNT_NOT_ADDED_YET
    BusAuthSDK.NETWORK_ERROR
```

#### Manual passenger ride verification
```
int result = busAuthSDK.passengerManualVerification()
result:
    BusAuthSDK.SUCCESS
    BusAuthSDK.ACCOUNT_NOT_ADDED_YET
```

#### Create a new route for a Driver
```
int result = busAuthSDK.driverNewRoute(String driverId,
                                       List<BusStop> busStops)
/**
 * busStops is a list containing busStop objects
 * BusStop busStop = new BusStop(String boardingTime, Location gpsLocation)
 */
result:
    BusAuthSDK.SUCCESS
    BusAuthSDK.ACCOUNT_NOT_ADDED_YET
```

#### Stop and Close BusAuthSDK
```
busAuthSDK.stop();
busAuthSDK.close();
```


## SDK events
```
BusAuthEventListener eventListener = new BusAuthEventListener(context) {
        @Override
        public void onPassengerStartBoarding(String passengerId,
                                             String driverId,
                                             String boardingTime) {
            /**
             * onPassengerStartBoarding is called on the Passenger's account 
             * when the Bus arrived to the stop.
             */
        }
        
        @Override
        public void onPassengerVerificationResult(int passengerRideResult) {
            /**
             * onPassengerVerificationResult is called on the Passenger's account 
             * as a result of Ride verification.
             * passengerRideResult:
             *      BusAuthSDK.RIDE_VERIFIED
             *      BusAuthSDK.RIDE_NOT_VERIFIED
             */       
        }
        
        @Override
        public void onPassengerRequireManualVerification() {
            /**
             * onPassengerRequireManuallVerification is called on the Passenger's account 
             * when the Passenger need to start verification manually.
             * To do this, use the busAuthSDK.passengerManualVerification() method
             */     
        }
        
        @Override
        public void onBusArrived(String passengerId,
                                 String driverId,
                                 String boardingTime) {
            /**
             * onBusArrived is called on the Drivers's account 
             * when the Bus arrived to the stop.
             */
        }
        
        @Override
        public void onUpdateVerifiedPassengers(int countVerifiedPassengers,
                                               int countAllPassengers,
                                               String[] verifiedPassengerIDs) {
            /**
             * onUpdateVerifiedPassengers is called on the Drivers's account 
             * every time a Passenger is verified.
             */
        }

        @Override
        public void onManualVerificationResult(int rideResult) {
            /**
             * onManualVerificationResult is called on the Driver's account 
             * as a result of a Passenger manual verification.
             * rideResult:
             *      BusAuthSDK.PASSENGER_VERIFIED
             *      BusAuthSDK.PASSENGER_NOT_VERIFIED
             */
        }    
    };
busAuthSDK.setEventListener(eventListener);
```
